<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">

    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/orders-list.css" />
</head>

<body>
<?php include(dirname(__DIR__).'/header.php'); ?>

  <div class="pw-order-list-container">
      <div class="pw-card pw-card-order-list">
        <div class="pw-card-title"><b>Lista zleceń</b><br/></div>
          <div class="pw-card-content pw-card-content-order-list">

        <!-- Grid with orders -->
          <form action="?page=order-details" method="POST">
          <?php 
            if (isset($orders) && count($orders) > 0) {
              foreach ($orders as $order) {
                echo '<div class="pw-card pw-card-offer">
                        <div class="pw-card-row">
                          <div class="pw-order-section">
                            Typ zwierzecia: <span class="pw-order-details">'.$order->petType.'</span><br />
                            Agresywne/Nieposłuszne: <span class="pw-order-details">'.$order->isPetAggressive.'</span><br />
                            Typ zlecenia: <span class="pw-order-details">'.$order->jobType.'</span><br />
                          </div>
                          <div class="pw-order-section">
                            Adres: <span class="pw-order-details">'.$order->address.'</span><br />
                            Od: <span class="pw-order-details">'.$order->startDate.'&nbsp;'.$order->startHour.'</span><br />
                            Do: <span class="pw-order-details">'.$order->endDate.'&nbsp;'.$order->endHour.'</span><br />
                          </div>
                          <div class="pw-order-section">
                            Cena: <span class="pw-order-details">'.$order->price.' PLN</span><br />
                            Status: <span class="pw-order-details">'.$order->offerStatus.'</span><br />';
                  if ($order->offerStatus === 'Zaakceptowane') {
                    echo 'Nr telefonu: <span class="pw-order-details">'.$order->phoneNumber.'</span><br />';
                  }
                  echo '</div></div>';

                  if ($order->offerStatus === 'Nowe') {
                    echo '<div class="pw-button-control">
                    <button class="pw-button pw-button-reject" type="submit" name="action_id" value="rejected_'.$order->getId().'" >Odrzuć</button>
                    <button class="pw-button pw-button-accept" type="submit" name="action_id" value="accepted_'.$order->getId().'_'.$order->phoneNumber.'">Przyjmij</button>
                          </div>';
                  }
                  echo '</div>';
                } 
            } else {
              echo 'Wygląda na to, że jeszcze nie masz żadnych zleceń <i class="fa fa-surprise"></i>';
            }
          ?>
          </form>
        </div>
      </div> 

  </div>

</body>
