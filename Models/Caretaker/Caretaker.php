<?php

require_once 'Restriction.php';
require_once 'Stake.php';
require_once 'WorkSchedule.php';
require_once __DIR__.'//..//User.php';


class Caretaker {
  private Restriction $restriction;
  private Stake $stake;
  private array $workSchedules;
  private string $nickname;
  private User $user;
  private bool $active;

  public function __construct(Restriction $restriction, Stake $stake, array $workSchedules, string $nickname, User $user, bool $active) {
    $this->restriction = $restriction;
    $this->stake = $stake;
    $this->workSchedules = $workSchedules;
    $this->nickname = $nickname;
    $this->user = $user;
    $this->active = $active;
  }

  public function getNickname(): string {
    return $this->nickname;
  }

  public function getRestriction(): Restriction {
    return $this->restriction;
  }

  public function getStake(): Stake {
    return $this->stake;
  }

  public function getWorkSchedules(): array {
    return $this->workSchedules;
  }

  public function getUser(): User {
    return $this->user;
  }

  public function getActive(): bool {
    return $this->active;
  }
}


?>