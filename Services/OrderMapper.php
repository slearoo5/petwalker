<?php

require_once __DIR__.'//..//Models//Order.php';

class OrderMapper {
  public function mapToDate(): Date {
    $date = Date::build()
              ->withStartDay($_POST['startDate'])
              ->withEndDay($_POST['endDate'] ?: $_POST['startDate'])
              ->withStartHour($_POST['startTime'])
              ->withEndHour($_POST['endTime'] ?: $_POST['startTime']);

    return $date;
  }

  public function mapToCaretaker(): Caretaker {
    $caretakerRepository = new CaretakerRepository();
    $caretaker = $caretakerRepository->getCaretakerById($_POST['idUser']);

    return $caretaker;
  }

  public function  mapToOwner(): Owner {
    $owner = Owner::build()
              ->withFirstName($_POST['firstName'])
              ->withSurname($_POST['surname'])
              ->withHomeAddress($_POST['address'])
              ->withPhoneNumber($_POST['phoneNumber'])
              ->withEmail($_POST['email']);

    return $owner;
  }

  public function mapPet(): Object {
    switch ($_POST['petType']) {
      case 'Dog':
        return $this->mapPetToDog();
      break;

      case 'Cat':
        return $this->mapPetToCat();
      break;

      case 'TerrariumPet':
        return $this->mapPetToTerrariumPet();
      break;

      case 'AquariumPet':
        return $this->mapPetToAquariumPet();
      break;
    }
  }

  public function mapPetToDog(): Dog {
    $dog = Dog::build()
            ->setName($_POST['name'])
            ->setBreed($_POST['breed'])
            ->setSex($_POST['sex'])
            ->setWeight($_POST['weight'])
            ->setIsAgressive($_POST['aggressive']);

    return $dog;
  }

  public function mapPetToCat(): Cat {
    $cat = Cat::build()
            ->setName($_POST['name'])
            ->setBreed($_POST['breed'])
            ->setSex($_POST['sex'])
            ->setWeight($_POST['weight'])
            ->setIsObedient($_POST['aggressive']);

    return $cat;
  }

  public function mapPetToTerrariumPet(): TerrariumPet {
    $terrariumPet = TerrariumPet::build()
                      ->setName($_POST['name'])
                      ->setPetType($_POST['breed']);

    return $terrariumPet;
  }

  public function mapPetToAquariumPet(): AquariumPet {
    $aquariumPet = AquariumPet::build()
                    ->setPetType($_POST['breed']);
    return $aquariumPet;
  }

  public function mapToOrderArray(array $orders): array {
    $mappedOrders = [];
    foreach ($orders as $order) {
      // caretaker
      $p = explode(' ', $order['pet_type']);
      $s = explode(' ', $order['service_type']);

      $restr = Restriction::build()
                ->withPetTypes($p)
                ->withServiceTypes($s)
                ->withAggressivePets($order['aggressive_pet'])
                ->withMaxWeight($order['max_pet_weight']);

      $stake = Stake::build()
                ->withFullCare($order['stake_full_care'])
                ->withWalk($order['stake_walk'])
                ->withService($order['stake_service'])
                ->withAggressivePenalty($order['stake_aggressive_pet']);

      $nickname = '';
      $workSchedules = [];
      $user = new User('', '');
      $caretaker = new Caretaker($restr, $stake, $workSchedules, $nickname, $user, !!$order['active']);

      // owner
      $owner = Owner::build()
                  ->withFirstName($order['first_name'])
                  ->withSurname($order['surname'])
                  ->withHomeAddress($order['address'])
                  ->withPhoneNumber($order['phone_number'])
                  ->withEmail($order['email'])
                  ->withId($order['id_owner']);

      // pet
      $pet = $this->mapToPet($order['animal_type'], $order);

      // date
      $date = Date::build()
                ->withStartDay($order['day_start'])
                ->withEndDay($order['day_end'])
                ->withStartHour($order['hour_start'])
                ->withEndHour($order['hour_end']);

      $price = $order['price'];
      $service = $order['job_type'];
      $status = $order['status'];

      $mappedOrder = Order::build()
                      ->withPet($pet)
                      ->withOwner($owner)
                      ->withCaretaker($caretaker)
                      ->withDate($date)
                      ->withPrice($price)
                      ->withServiceType($service)
                      ->withStatus($status)
                      ->withId($order['id_order']);

      $mappedOrders[] = $mappedOrder;
    }

    return $mappedOrders;
  }

  private function mapToPet($petType, $order) {
    switch ($petType) {
      case 'Dog':
        $pet = $this->mapToDog($order);
      break;

      case 'Cat':
        $pet = $this->mapToCat($order);
      break;

      case 'TerrariumPet':
        $pet = $this->mapToTerrariumPet($order);
      break;

      case 'AquariumPet':
        $pet = $this->mapToAquariumPet($order);
      break;
    }

    return $pet;
  }

  private function mapToDog($order): Dog {
    $dog = Dog::build()
            ->setName($order['name'])
            ->setBreed($order['breed'])
            ->setWeight($order['weight'])
            ->setIsAgressive($order['aggressive']);

    return $dog;
  }

  private function mapToCat($order): Cat {
    $cat = Cat::build()
            ->setName($order['name'])
            ->setBreed($order['breed'])
            ->setWeight($order['weight'])
            ->setIsObedient($order['aggressive']);

    return $cat;
  }

  private function mapToTerrariumPet($order): TerrariumPet {
    $terrariumPet = TerrariumPet::build()
                      ->setName($order['name'])
                      ->setPetType($order['animal_type']);

    return $terrariumPet;
  }

  private function mapToAquariumPet($order): AquariumPet {
    $aquariumPet = AquariumPet::build()
                    ->setPetType($order['animal_type']);
    return $aquariumPet;
  }

}

?>