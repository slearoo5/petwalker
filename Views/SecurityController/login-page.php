<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/login-page.css" />
</head>

<body>
  <?php include(dirname(__DIR__).'/header.php'); ?>

  <div class="pw-login-page-container">
    <div class="pw-card pw-card-login-page">
      <div class="pw-card-title pw-card-title-login-page">Cieszymy się z Twojego powrotu</div>
      <div class="pw-card-subtitle pw-card-subtitle-login-page">Zaloguj się i szczekaj z radości!</div>
      <div class="pw-card-content pw-card-content-login-page">
        <form action="?page=login-page" method="POST">

          E-mail<br> 
          <input class="pw-input pw-login-input" type="email" name="email" required /><br><br>
          Hasło<br> 
          <input class="pw-input pw-login-input" type="password" name="password" required /><br><br>

          <!-- @TODO: Adjust error/warning/info messages -->
          <?php
            if(isset($messages)){
              foreach($messages as $message) {
                echo '<span style="color:red">'.$message.'</span><br>';
              }
            }
          ?>

          <div class="pw-button-position">
            <div class="pw-login-page-link">Nie masz konta?&nbsp; <a href="?page=register-page" class="pw-link">Zarejestruj się <i class="fa fa-arrow-right"></i></a></div>
            <input type="submit" class="pw-button pw-login-page-button" value="Zaloguj się"></input>              
          </div>

        </form>
      </div>
    </div>

  </div>
</body>