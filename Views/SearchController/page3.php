<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../../Public/Styles/search-form.css" />

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="../Public/JavaScript/search-form.js"></script>
</head>

<body>
    <?php include(dirname(__DIR__).'/header.php'); ?>

    <div class="pw-search-form-container">
        <div class="pw-card pw-search-form-card pw-search-form-card-step-three">
            <div class="pw-card-title">Krok 3/3</div>
            <div class="pw-card-subtitle">
                Podaj swoje informacje (informacje dotyczące właściciela)
            </div>
            <div class="pw-card-content pw-search-form-card-content">
                <form action="?page=search-results" method="GET">
                    <!-- Adding hidden input to concat url query parameters -->
                    <?php 
                      foreach ($_GET as $key => $value) {
                        echo "<input type='hidden' name=$key value=$value />";
                      }
                    ?>


                    Imię<br> 
                    <input class="pw-input" type="text" name="firstName" required /><br><br>
                    Nazwisko<br> 
                    <input class="pw-input" type="text" name="surname" required /><br><br> 
                    
                    Adres<br> 
                    <input class="pw-input" type="text" name="address" required /><br><br>
                    Telefon<br> 
                    <input class="pw-input" type="tel" max-lenght="15" name="phoneNumber" required /><br><br>
                    E-mail<br> 
                    <input class="pw-input" type="email" name="email" required/><br><br>

                    <div class="pw-button-position">
                      <input type="button" value="Wstecz" onclick="history.back()" class="pw-button pw-search-form-back-button" />
                      <button type="submit" name="page" value="search-results" class="pw-button pw-search-form-next-button">Wyszukaj</button></a>              
                </div>
                </form>

            </div>
            </div>
    </div>
</body>

