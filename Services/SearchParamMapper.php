<?php

require_once __DIR__.'//..//Models//Caretaker//Restriction.php';
require_once __DIR__.'//..//Models//Pets//Dog.php';
require_once __DIR__.'//..//Models//Pets//Cat.php';
require_once __DIR__.'//..//Models//Pets//TerrariumPet.php';
require_once __DIR__.'//..//Models//Pets//AquariumPet.php';


class SearchParamMapper {
  public function getRestrictionParams($petType): Restriction {    
    switch ($petType) {
      case 'Dog':
        $object = $this->mapFromUrlToDog();
        $restriction = Restriction::build()
                        ->withPetTypes(['Dog'])
                        ->withServiceTypes([$_GET['serviceType']])
                        ->withAggressivePets($object->isAgressive())
                        ->withMaxWeight($object->getWeight());
      break;

      case 'Cat':
        $object = $this->mapFromUrlToCat();
        $restriction = Restriction::build()
                        ->withPetTypes(['Cat'])
                        ->withServiceTypes([$_GET['serviceType']])
                        ->withAggressivePets($object->isObedient())
                        ->withMaxWeight($object->getWeight());
      break;

      case 'TerrariumPet':
        $object = $this->mapFromUrlToTerrariumPet();
        $restriction = Restriction::build()
                        ->withPetTypes(['TerrariumPet'])
                        ->withServiceTypes([$_GET['serviceType']])
                        ->withAggressivePets(false)
                        ->withMaxWeight(0);
        
      break;

      case 'AquariumPet':
        $object = $this->mapFromUrlToAquariumPet();
        $restriction = Restriction::build()
                        ->withPetTypes(['AquariumPet'])
                        ->withServiceTypes([$_GET['serviceType']])
                        ->withAggressivePets(false)
                        ->withMaxWeight(0);
      break;
    }

    return $restriction;
  }

  private function mapFromUrlToDog(): Dog {
    $dog = Dog::build()
            ->setName($_GET['name'])
            ->setBreed($_GET['breed'])
            ->setSex($_GET['sex'])
            ->setWeight($_GET['weight'])
            ->setIsAgressive($_GET['aggressive']);

    return $dog;
  }

  private function mapFromUrlToCat(): Cat {
    $cat = Cat::build()
            ->setName($_GET['name'])
            ->setBreed($_GET['breed'])
            ->setSex($_GET['sex'])
            ->setWeight($_GET['weight'])
            ->setIsObedient($_GET['aggressive']);

    return $cat;
  }

  private function mapFromUrlToTerrariumPet(): TerrariumPet {
    $terrariumPet = TerrariumPet::build()
                      ->setName($_GET['name'])
                      ->setPetType($_GET['breed']);

    return $terrariumPet;
  }

  private function mapFromUrlToAquariumPet(): AquariumPet {
    $aquariumPet = AquariumPet::build()
                    ->setPetType($_GET['breed']);
    return $aquariumPet;
  }

}

?>