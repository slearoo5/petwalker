<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/page-not-found.css" />
</head>

<body>
<?php include(dirname(__DIR__).'/header.php'); ?>
  
  <div class="pw-page-not-found-container">
    <div class="pw-card pw-page-not-found-card">
      <div class="pw-card-title pw-card-title-page-not-found">
          Węch zawiódł?
      </div>
      <div class="pw-card-subtitle pw-card-subtitle-page-not-found">
          Błąd 404 - podany adres URL nie istnieje<br>
          lub został zmieniony...
      </div>
      <form method="GET">
        <div class="pw-card-content pw-card-content-page-not-found">
          <button type="submit" name="?page" value="landing-page" class="pw-button pw-page-not-found-button">Wróć na stronę główną</button>
        </div>
      </form>
    </div>
  </div>
</body>