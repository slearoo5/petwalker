<?php

class WorkSchedule {
  private string $day;
  private bool $allDay;
  private string $startHour = '';
  private string $endHour = '';

  public function __construct() {
    $this->day = '';
    $this->allDay = false;
    $this->startHour = '';
    $this->endHour = '';
  }

  public static function build() {
    $instance = new self();
    return $instance;
  }

  public function withDay(string $day) {
    $this->day = $day;
    return $this;
  }

  public function withAllDay(bool $allDay) {
    $this->allDay = $allDay;
    return $this;
  }

  public function withStartHour(string $startHour) {
    $this->startHour = $startHour;
    return $this;
  }

  public function withEndHour(string $endHour) {
    $this->endHour = $endHour;
    return $this;
  }

  public function getDay(): string {
    return $this->day;
  }

  public function getAllDay(): bool {
    return $this->allDay;
  }

  public function getStartHour(): string {
    return $this->startHour;
  }

  public function getEndHour(): string {
    return $this->endHour;
  }
}

?>