<?php


class Cat {
  private $name;
  private $breed;
  private $sex;
  private $weight;
  private $obedient;
  private ?int $id;

  public function __construct() { }

  public static function build(): Cat {
    $instance = new self();
    return $instance;
  }

  public function getId(): int {
    return $this->id;
  }

  public function getName(): string {
    return $this->name;
  }

  public function getBreed(): string {
    return $this->breed;
  }

  public function getSex(): string {
    return $this->sex;
  }

  public function getWeight(): int {
    return $this->weight;
  }

  public function isObedient(): bool {
    return $this->obedient;
  }

  public function setName(string $name) {
    $this->name = $name;
    return $this;
  }

  public function setBreed(string $breed) {
    $this->breed = $breed;
    return $this;
  }

  public function setSex(string $sex) {
    $this->sex = $sex;
    return $this;
  }

  public function setWeight(int $weight) {
    $this->weight = $weight;
    return $this;
  }

  public function setIsObedient(bool $obedient) {
    $this->obedient = $obedient;
    return $this;
  }

  public function withId(int $id) {
    $this->id = $id;
    return $this;
  }
}

?>