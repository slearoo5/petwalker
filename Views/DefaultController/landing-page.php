<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/landing-page.css" />
</head>
<body>
  <?php include(dirname(__DIR__).'/header.php'); ?>

  <div class="pw-landing-page-container">
    <div class="pw-card pw-landing-page-card">
      <div class="pw-card-title pw-card-title-landing-page">Znajdź idealnego opiekuna</div>
      <div class="pw-card-subtitle pw-card-subtitle-landing-page">
          Wystarczy, że wypełnisz krótki formularz,<br>
          a otrzymasz listę opiekunów<br>
          gotowych zaopiekować się Twoim zwierzakiem!
      </div>
      <div class="pw-card-content pw-card-content-landing-page">
            Jakie zwierzęciem mamy się zająć?<br>
        
        <form action="?page=search-form" method="GET">
          <input type="hidden" name="page" value="search-form" />
          <select name="petType" class="pw-select pw-landing-page-select">
            <option value="Dog">Pies</option>
            <option value="Cat">Kot</option>
            <option value="TerrariumPet">Zwierzę w terrarium</option>
            <option value="AquariumPet">Zwierzę w akwarium</option>
          </select>

          <br />
          <a href="?page=search-form">
            <button type="submit" class="pw-button pw-landing-page-button" href="?page=search-form&pet-type=cat">
              Dalej <i class="fas fa-arrow-right"></i>
            </button>
          </a>
        </form>

      </div>
    </div>
  </div>

  <?php readfile(dirname(__DIR__).'/DefaultController/how-it-works.html'); ?>

</body>
