<?php

class Stake {
  private int $fullCare;
  private int $walk;
  private int $service;
  private int $aggressivePenalty;

  public function __construct() {
    $this->fullCare = 0;
    $this->walk = 0;
    $this->service = 0;
    $this->aggressivePenalty = 0;
  }

  public static function build() {
    $instance = new self();
    return $instance;
  }

  public function withFullCare(int $stake) {
    $this->fullCare = $stake;
    return $this;
  }

  public function withWalk(int $stake) {
    $this->walk = $stake;
    return $this;
  }

  public function withService(int $stake) {
    $this->service = $stake;
    return $this;
  }

  public function withAggressivePenalty(int $stake) {
    $this->aggressivePenalty = $stake;
    return $this;
  }

  public function getFullCare(): int {
    return $this->fullCare;
  }

  public function getWalk(): int {
    return $this->walk;
  }

  public function getService(): int {
    return $this->service;
  }

  public function getAggressivePenalty(): int {
    return $this->aggressivePenalty;
  }
}

?>