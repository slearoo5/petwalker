<?php


class TerrariumPet {
  private $name;
  private $type;
  private ?int $id;

  public function __construct() { }

  public static function build(): TerrariumPet {
    $instance = new self();
    return $instance;
  }

  public function getId(): int {
    return $this->id;
  }

  public function getName(): string {
    return $this->name;
  }

  public function getPetType(): string {
    return $this->type;
  }

  public function setName(string $name) {
    $this->name = $name;
    return $this;
  }

  public function setPetType(string $petType) {
    $this->type = $petType;
    return $this;
  }

  public function withId(int $id) {
    $this->id = $id;
    return $this;
  }
}

?>