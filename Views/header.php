<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/header.css" />

    <!-- FontAwesome -->
    <script defer src="../Public/Resources/fontawesome-free-5.11.2-web/js/all.js"></script>
    <link href="../Public/Resources/fontawesome-free-5.11.2-web/css/all.css" rel="stylesheet">
    
    <!-- Text fonts -->
    <link href="https://fonts.googleapis.com/css?family=Pangolin&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Cabin+Sketch&display=swap" rel="stylesheet">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="../Public/JavaScript/header.js"></script>
</head>
<body>



    <header>
        <div class="pw-header-container">
            <a class="pw-header-title" href="?page=landing-page">
                    PetWalker <img src="../Public/Resources/paw_brown.svg" class="pw-logo" alt="paw" />
            </a>
            <ul class="pw-header-nav">
                <a href="?page=landing-page#how-it-works" onclick="animeHowItWorksCards()"> <li>Jak to działa?</li></a>
                <?php if(!isset($_SESSION['id'])) { ?>
                  <a href="?page=login-page"><li>Zaloguj się</li></a>
                <?php } else { ?>
                  <a href="?page=orders-list"><li>Twoje zlecenia</li></a>
                  <a href="?page=settings-caretaker"><li>Opcje</li></a>
                  <a href="?page=logout"><li>Wyloguj się</li></a>
                <?php } ?>
            </ul>

            <a href="javascript:void(0);" class="pw-hamburger-menu-icon" onclick="showMenu()">
                <i class="fa fa-bars"></i>
            </a>
        </div>
        
        <ul id="pw-mobile-nav">
            <a href="?page=landing-page#how-it-works"><li><i class="fa fa-question"></i>&nbsp; Jak to działa?</li></a>
            <?php
                if(!isset($_SESSION['id'])) {
                    echo '<a href="?page=login-page"><li><i class="fa fa-sign-in-alt"></i>&nbsp; Zaloguj się</li></a>';
                } else {
                  echo '<a href="?page=orders-list"><li><i class="fas fa-list"></i>&nbsp; Twoje zlecenia</li></a>';
                  echo '<a href="?page=settings-caretaker"><li><i class="fa fa-hand-point-left"></i>&nbsp; Opcje</li></a>';
                  echo '<a href="?page=logout"><li><i class="fa fa-sign-in-alt"></i>&nbsp; Wyloguj się</li></a>';
                }
            ?>
        </ul>

    </header>

</body>
