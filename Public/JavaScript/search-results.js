function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split('&'),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
  }
};

function updateDatetime(value) {
  var $sDate = $('input[name=startDate]');
  var $sTime = $('input[name=startTime]');
  var $eDate = $('input[name=endDate]');
  var $eTime = $('input[name=endTime]');
  var date = new Date();
  var today = formatDate(date);

  if ($sDate.val() < today) {
    $sDate.val(today);
  }

  if ($sDate.val() === today && $sTime.val() < formatTime(new Date(date.getTime() + 60*60000))) {
    $sTime.val(formatTime(new Date(date.getTime() + 60*60000)));
  }
  
  if ($eDate.val() < $sDate.val()) {
    $eDate.val($sDate.val());
  }

  if ($eDate.val() === $sDate.val()) {
    if ($eTime.val() < $sTime.val()) {
      $eTime.val($sTime.val());
    }
  }
}


function formatDate(date) {
  var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

  if (month.length < 2) 
      month = '0' + month;
  if (day.length < 2) 
      day = '0' + day;

  return [year, month, day].join('-');
} 


function formatTime(date) {
  var time = date.toLocaleTimeString().slice(0, 3) + '00';

  return time;
}


function getCaretakers() {
  var xmlHttp = new XMLHttpRequest();
  var $sDate = $('input[name=startDate]').val();
  var $sTime = $('input[name=startTime]').val();
  var $eDate = $('input[name=endDate]').val();
  var $eTime = $('input[name=endTime]').val();

  var startDay = new Date($sDate).toDateString().slice(0, 3);
  $sDate = startDay + '_' + $sDate;
  
  var endDay = new Date($eDate).toDateString().slice(0, 3);
  $eDate =  endDay + '_' + $eDate;

  var url = 'Views/caretaker-list.php?startDate=' + $sDate + '&startTime=' + $sTime + '&endDate=' + $eDate + '&endTime=' + $eTime;

  xmlHttp.open('GET', url, false);
  console.log(url);

  xmlHttp.send(null);
  
  $('#tmp').html(xmlHttp.responseText);
  $('.pw-card-content .pw-card-row').fadeIn(800);

}


// Set default time
$(document).ready(function() {
  var date = new Date();
  var today = formatDate(date);
  var maxDate = date.getTime() + 129600*60000;

  $('input[name=startDate]').val(today);
  $('input[name=startDate]').attr({'min': today, 'max': formatDate(new Date(maxDate))});
  $('input[name=startTime]').val(formatTime(new Date(date.getTime() + 60*60000)));
  $('input[name=endDate]').val(today);
  $('input[name=endDate]').attr({'min': today, 'max': formatDate(new Date(maxDate))});

  $('input[name=endTime').val(formatTime(new Date(date.getTime() + 120*60000)));

  var service = getUrlParameter('serviceType');
  
  // @TODO add updating endTime and endDate based on any user input (endDate >= startDate etc.)
  switch (service) {
    case 'service':
    case 'walk':
      $('#end').hide();
      $('input[name=endDate]').prop('disabled', true).hide();
      $('input[name=endTime').prop('disabled', true).hide();
      break;
  }

});