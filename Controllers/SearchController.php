<?php 

require_once 'AppController.php';

class SearchController extends AppController {

  public function search() {
    if (!isset($_GET['petType'])) {
      $this->render('page1');
    } else {
      if (!isset($_GET['step2'])) {
        $this->render($_GET['petType']);
      } else {
        $this->render('page3');
      }
    }
  }
}

?>