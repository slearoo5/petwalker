<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/register-page.css" />
</head>

<body>
  <?php include(dirname(__DIR__).'/header.php'); ?>

  <div class="pw-register-page-container">
    <div class="pw-card pw-card-register-page">
      <div class="pw-card-title pw-card-title-register-page">Jesteś tu nowy?</div>
      <div class="pw-card-subtitle pw-card-subtitle-register-page">Stwórz konto i dołącz do naszej zwierzęcej społeczności!</div>
      <div class="pw-card-content pw-card-content-register-page">
        <form action="?page=register-page" method="POST">

          E-mail<br> 
          <input class="pw-input" type="email" name="email" required /><br><br>
          Hasło<br> 
          <input class="pw-input" type="password" name="password" minlength="6" required /><br><br>
          Powtórz hasło<br> 
          <input class="pw-input" type="password" name="passwordConfirmation" minlength="6" required /><br><br>

          <!-- @TODO: Adjust error/warning/info messages -->
          <?php
            if(isset($messages)){
              foreach($messages as $message) {
                echo '<span style="color:red">'.$message.'</span><br>';
              }
            }
          ?>

          <div class="pw-button-position">
            <div class="pw-register-page-link">Masz już konto?&nbsp; <a href="?page=login-page" class="pw-link">Zaloguj się <i class="fa fa-arrow-right"></i></a></div>
            <input type="submit" class="pw-button pw-register-page-button" value="Zarejestruj się"></input>              
          </div>

        </form>
      </div>
    </div>

  </div>
</body>