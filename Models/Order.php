<?php

require_once __DIR__.'//..//Models/Caretaker/Caretaker.php';
require_once __DIR__.'//..//Models/Date.php';
// require_once __DIR__.'//..//Models/Owner.php';


class Order {
  private $pet;
  private Owner $owner;
  private Caretaker $caretaker;
  private Date $date;
  private int $price;
  private string $serviceType;
  private ?int $id;
  private string $status;

  public function build(): Order {
    $instance = new self();
    return $instance;
  }

  public function withPet($pet) {
    $this->pet = $pet;
    return $this;
  }

  public function withOwner(Owner $owner) {
    $this->owner = $owner;
    return $this;
  }

  public function withCaretaker(Caretaker $caretaker) {
    $this->caretaker = $caretaker;
    return $this;
  }

  public function withDate(Date $date) {
    $this->date = $date;
    return $this;
  }

  public function withPrice(int $price) {
    $this->price = $price;
    return $this;
  }

  public function withServiceType(string $service) {
    $this->serviceType = $service;
    return $this;
  }

  public function withId(int $id) {
    $this->id = $id;
    return $this;
  }

  public function withStatus(string $status) {
    $this->status = $status;
    return $this;
  }

  public function getPet() {
    return $this->pet;
  }

  public function getOwner(): Owner {
    return $this->owner;
  }

  public function getCaretaker(): Caretaker {
    return $this->caretaker;
  }

  public function getDate(): Date {
    return $this->date;
  }

  public function getPrice(): int {
    return $this->price;
  }

  public function getServiceType(): string {
    return $this->serviceType;
  }

  public function getId(): int {
    return $this->id;
  }

  public function getStatus(): string {
    return $this->status;
  }
}

?>