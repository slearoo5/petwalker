<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/success-page.css" />
</head>

<body>
<?php include(dirname(__DIR__).'/header.php'); ?>

    <div class="pw-success-page-container">
      <p class="pw-success-page-title">Pomyślnie wysłano ofertę do opiekuna! Poczekaj kilka chwil na telefon ze szczegółami <i class="fa fa-smile"></i></p>
    </div>

</body>
