<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/search-results.css" />

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script src="../Public/JavaScript/search-results.js"></script>

</head>

<body>
<?php include(dirname(__DIR__).'/header.php'); ?>

    <div class="pw-search-results-container">
      <form action="?page=success-page" method="POST">
        <!-- passing previous values -->
        <?php 
          foreach ($_GET as $key => $value) {
            echo "<input type='hidden' name=$key value=$value />";
          }
        ?>

        <div id="start" class="pw-card pw-card-search-results-container">
            <div class="pw-card pw-card-search-results-date">
                <div class="pw-card-title">Data rozpoczęcia</div>
                <div class="pw-card-subtitle"><i class="fa fa-arrow-right"></i></div>
                <div class="pw-card-content pw-card-search-results-settings">
                    <i class="fa fa-calendar"></i><input type="date" name="startDate" min="" max="" class="pw-date pw-date-start" onblur="getCaretakers()" onchange="updateDatetime(this.value)" />
                    <i class="fa fa-clock"></i><input type="time" name="startTime" step="3600" class="pw-date pw-date-start" onblur="getCaretakers()" onchange="updateDatetime(this.value)"/>
                </div>
            </div>

            <div id="end" class="pw-card pw-card-search-results-date">
                <div class="pw-card-title">Data zakończenia</div>
                <div class="pw-card-subtitle"><i class="fa fa-arrow-left"></i></div>
                <div class="pw-card-content pw-card-search-results-settings">
                    <i class="fa fa-calendar"></i><input type="date" name="endDate" min="" max="" class="pw-date pw-date-end" onblur="getCaretakers()" onchange="updateDatetime(this.value)"/>
                    <i class="fa fa-clock"></i><input type="time" name="endTime" step="3600" class="pw-date pw-date-end" onblur="getCaretakers()" onchange="updateDatetime(this.value)"/>
                </div>
            </div>

            <div class="pw-card-subtitle"><b>Lista ofert</b></div>
            <div class="pw-card-content" id="tmp">
            
            </div>

        </div>
      </form>
    </div>

</body>
