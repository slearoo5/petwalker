<?php 

require_once 'AppController.php';

class ErrorController extends AppController {

  public function handle() {
    $this->render('page-not-found');
  }
}

?>