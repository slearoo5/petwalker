<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../../Public/Styles/search-form.css" />

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="../Public/JavaScript/search-form.js"></script>
</head>

<body>
    <?php include(dirname(__DIR__).'/header.php'); ?>

    <div class="pw-search-form-container">
        <div class="pw-card pw-search-form-card pw-search-form-card-step-one">
            <div class="pw-card-title">Krok 1/3</div>
            <div class="pw-card-subtitle">
                Jaki zwierzak potrzebuje opieki?
            </div>
            <div class="pw-card-content pw-search-form-card-content">
                <form action="?page=search-form" method="GET">
                  <input type="hidden" name="page" value="search-form">
                    <div class="pw-animal-input">
                        <i class="fas fa-dog pw-animal-icon"></i><br>
                        <input type="radio" name="petType" value="Dog" required />Pies
                    </div>
                    <div class="pw-animal-input">
                        <i class="fas fa-cat pw-animal-icon"></i><br>
                        <input type="radio" name="petType" value="Cat" required />Kot
                    </div>
                    <div class="pw-animal-input">
                        <i class="fas fa-spider pw-animal-icon"></i><br>
                        <input type="radio" name="petType" value="TerrariumPet" required />Zwierzę w terrarium
                    </div>
                    <div class="pw-animal-input">
                        <i class="fas fa-fish pw-animal-icon"></i><br>
                        <input type="radio" name="petType" value="AquariumPet" required />Zwierzę w akwarium
                    </div>
                    <br>

                    <input type="submit" class="pw-button pw-search-form-confirm-button" value="Dalej" />
                </form>
            </div>
        </div>
    </div>
</body>