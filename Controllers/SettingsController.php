<?php 

require_once 'AppController.php';

class SettingsController extends AppController {
  private $caretaker;

  public function prepare() {
    $this->updateProfile();

    $caretakerRepository = new CaretakerRepository();
    $this->caretaker = $caretakerRepository->getCaretakerByEmail($_SESSION['email']);

    if (!!!$this->caretaker) {
      $userRepository = new UserRepository();
      $user = $userRepository->getUserById($_SESSION['id']);

      $this->caretaker = new Caretaker(new Restriction(), new Stake(), [], $user->getFirstName(), $user, false);
    }

    $this->render('settings-caretaker', ['user' => $this->caretaker]);
  }

  private function updateProfile() {
    $caretakerRepository = new CaretakerRepository();
    
    if (isset($_GET['section'])) {
      switch ($_GET['section']) {
        case 'personalData':
          $caretakerRepository->updatePersonalData();
        break;
  
        case 'preferences':
          $caretakerRepository->updatePreferences();
        break;
  
        case 'stakes':
          $caretakerRepository->updateStakes();
        break;
  
        case 'password':
          $caretakerRepository->updatePassword();
        break;
      }
    }
  }
}

?>