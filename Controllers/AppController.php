<?php

declare(strict_types=1);


abstract class AppController {

  private $request;

  public function __construct() {
    $this->request = $_SERVER['REQUEST_METHOD'];
    session_start();
  }

  protected function isGet(): bool {
    return $this->request === 'GET';
  }

  protected function isPost(): bool {
    return $this->request === 'POST';
  }

  protected function render(string $template = null, array $variables = []): void {
    $templatePath = $this->getTemplatePath($template);
    $output = 'File not found';

    if (file_exists($templatePath)) {
      extract($variables);

      ob_start();
      include $templatePath;
      $output = ob_get_clean();
    }

    print $output;
  }

  private function getTemplatePath(string $template = null): string {
    return $template ? dirname(__DIR__).'/Views/'.get_class($this).'/'.$template.'.php' : '';
  }

}

?>