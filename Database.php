<?php

require_once 'config.php';

class Database {
    private $username;
    private $password;
    private $host;
    private $database;

    public function __construct() {
        $this->username = PW_DB_USERNAME;
        $this->password = PW_DB_PASSWORD;
        $this->host = PW_DB_HOST;
        $this->database = PW_DB_NAME;
    }

    public function connect() {
        try {
          $connection = new PDO(
            "mysql:host=$this->host;dbname=$this->database", 
            $this->username,
            $this->password
          );

            // set the PDO error mode to exception
            $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            return $connection;
        }
        catch(PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }
}