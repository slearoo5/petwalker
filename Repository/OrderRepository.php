<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Order.php';
require_once __DIR__.'//..//Services//OrderMapper.php';
require_once "OwnerRepository.php";

class OrderRepository extends Repository {
  private OrderMapper $orderMapper;

  public function __construct() {
    parent::__construct();
    $this->orderMapper = new OrderMapper();
  }

  public function getOrdersByCaretakerId(int $userId): array {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $sqlStatement = $connection->prepare(
        'SELECT * FROM orders o 
          JOIN owners ow ON o.id_owner = ow.id_owner 
          JOIN pets p ON o.id_pet = p.id_pet 
          JOIN caretakers c ON o.id_caretaker = c.id_caretaker 
          JOIN date d ON o.id_date = d.id_date 
            WHERE c.id_user = :id'
      );
      $sqlStatement->bindParam(':id', $userId, PDO::PARAM_INT);
      $sqlStatement->execute();
      $result = $sqlStatement->fetchAll(PDO::FETCH_ASSOC);
      $orders = $this->orderMapper->mapToOrderArray($result);
      
      $connection->commit();
    } catch (PDOException $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }

    return $orders;
  }

  public function updateOrder(int $id, string $action) {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $statement = $connection->prepare("UPDATE `orders` SET `status` = :action WHERE id_order = :id");
      $statement->bindParam(':action', $action, PDO::PARAM_STR);
      $statement->bindParam(':id', $id, PDO::PARAM_INT);

      $statement->execute();
      $connection->commit();
    } catch (PDOException $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }
  }

  public function addNewOrder(Order $order) {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $this->insertNewOwner($order->getOwner(), $connection);
      $this->insertNewPet($order->getPet(), $order->getOwner()->getPhoneNumber(), $connection);
      $this->insertNewDate($order->getDate(), $connection);
      $ids = $this->getAllRequirementsForOrder($order, $connection);
  
      $sqlStatement = $connection->prepare(
        'INSERT INTO orders (id_owner, id_pet, id_caretaker, id_date, price, job_type, status)
          VALUES (:owner, :pet, :caretaker, :date, :price, :job, :status)'
      );
  
      $idOwner = ($ids[0]);
      $idDate = (int) $ids[1];
      $idPet = (int) $ids[2];
      $idCaretaker = (int) $ids[3];
    
      $sqlStatement->bindParam(':owner', $idOwner, PDO::PARAM_INT);
      $sqlStatement->bindParam(':date', $idDate, PDO::PARAM_INT);
      $sqlStatement->bindParam(':pet', $idPet, PDO::PARAM_INT);
      $sqlStatement->bindParam(':caretaker', $idCaretaker, PDO::PARAM_INT);
      $sqlStatement->bindParam(':price', $order->getPrice(), PDO::PARAM_INT);
      $sqlStatement->bindValue(':job', $order->getServiceType(), PDO::PARAM_STR);
      $sqlStatement->bindValue(':status', $order->getStatus() ?: 'pending', PDO::PARAM_STR);
  
      $sqlStatement->execute();
      $connection->commit();
    } catch (PDOException $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }
  }

  private function insertNewDate(Date $date, PDO $connection) {
    $sqlStatement = $connection->prepare(
      'INSERT INTO `date`(`day_start`, `day_end`, `hour_start`, `hour_end`)
        VALUES (:startDay, :endDay, :startTime, :endTime)'
    );

    $sqlStatement->bindParam(':startDay', $date->getStartDay(), PDO::PARAM_STR);
    $sqlStatement->bindParam(':endDay', $date->getEndDay(), PDO::PARAM_STR);
    $sqlStatement->bindParam(':startTime', $date->getStartHour(), PDO::PARAM_STR);
    $sqlStatement->bindParam(':endTime', $date->getEndHour(), PDO::PARAM_STR);

    $sqlStatement->execute();
  }

  private function insertNewPet($pet, string $ownerPhoneNumber, PDO $connection) {

    $petType = get_class($pet);
    $ownerRepo = new OwnerRepository();
    $owner = $ownerRepo->getOwnersByPhoneNumber($ownerPhoneNumber)[0];

    switch ($petType) {
      case 'Dog':
        $this->insertNewDog($pet, $owner->getId(), $connection);
      break;

      case 'Cat':
        $this->insertNewCat($pet, $owner->getId(), $connection);
      break;

      case 'TerrariumPet':
        $this->insertNewTerrariumPet($pet, $owner->getId(), $connection);
      break;

      case 'AquariumPet':
        $this->insertNewAquariumPet($pet, $owner->getId(), $connection);
      break;
    }
  }

  private function insertNewDog(Dog $pet, int $idOwner, PDO $connection) {
    $sqlStatement = $connection->prepare(
      'INSERT INTO `pets`(`animal_type`, `breed`, `weight`, `aggressive`, `id_owner`, `name`)
        VALUES (:petType, :breed, :weight, :isAggressive, :idOwner, :name)'
    );
    $sqlStatement->bindValue(':petType', 'Dog', PDO::PARAM_STR);
    $sqlStatement->bindParam(':breed', $pet->getBreed(), PDO::PARAM_STR);
    $sqlStatement->bindParam(':weight', $pet->getWeight(), PDO::PARAM_INT);
    $sqlStatement->bindParam(':isAggressive', $pet->isAgressive(), PDO::PARAM_BOOL);
    $sqlStatement->bindParam(':idOwner', $idOwner, PDO::PARAM_INT);
    $sqlStatement->bindParam(':name', $pet->getName(), PDO::PARAM_STR);

    $sqlStatement->execute();
  }

  private function insertNewCat(Cat $pet, int $idOwner, PDO $connection) {
    $sqlStatement = $connection->prepare(
      'INSERT INTO `pets`(`animal_type`, `breed`, `weight`, `aggressive`, `id_owner`, `name`)
        VALUES (:petType, :breed, :weight, :isAggressive, :idOwner, :name)'
    );
    $sqlStatement->bindValue(':petType', 'Cat', PDO::PARAM_STR);
    $sqlStatement->bindParam(':breed', $pet->getBreed(), PDO::PARAM_STR);
    $sqlStatement->bindParam(':weight', $pet->getWeight(), PDO::PARAM_INT);
    $sqlStatement->bindParam(':isAggressive', $pet->isObedient(), PDO::PARAM_BOOL);
    $sqlStatement->bindParam(':idOwner', $idOwner, PDO::PARAM_INT);
    $sqlStatement->bindParam(':name', $pet->getName(), PDO::PARAM_STR);

    $sqlStatement->execute();
  }

  private function insertNewTerrariumPet(TerrariumPet $pet, int $idOwner, PDO $connection) {
    $sqlStatement = $connection->prepare(
      'INSERT INTO `pets`(`animal_type`, `breed`, `weight`, `aggressive`, `id_owner`, `name`)
        VALUES (:petType, :breed, :weight, :isAggressive, :idOwner, :name)'
    );
    $sqlStatement->bindValue(':petType', 'TerrariumPet', PDO::PARAM_STR);
    $sqlStatement->bindParam(':breed', $pet->getPetType(), PDO::PARAM_STR);
    $sqlStatement->bindValue(':weight', 0, PDO::PARAM_INT);
    $sqlStatement->bindValue(':isAggressive', 0, PDO::PARAM_BOOL);
    $sqlStatement->bindParam(':idOwner', $idOwner, PDO::PARAM_INT);
    $sqlStatement->bindParam(':name', $pet->getName(), PDO::PARAM_STR);

    $sqlStatement->execute();
  }

  private function insertNewAquariumPet(AquariumPet $pet, int $idOwner, PDO $connection) {
    $sqlStatement = $connection->prepare(
      'INSERT INTO `pets`(`animal_type`, `breed`, `weight`, `aggressive`, `id_owner`, `name`)
        VALUES (:petType, :breed, :weight, :isAggressive, :idOwner, :name)'
    );
    $sqlStatement->bindValue(':petType', 'AquariumPet', PDO::PARAM_STR);
    $sqlStatement->bindParam(':breed', $pet->getPetType(), PDO::PARAM_STR);
    $sqlStatement->bindValue(':weight', 0, PDO::PARAM_INT);
    $sqlStatement->bindValue(':isAggressive', 0, PDO::PARAM_BOOL);
    $sqlStatement->bindParam(':idOwner', $idOwner, PDO::PARAM_INT);
    $sqlStatement->bindValue(':name', '', PDO::PARAM_STR);

    $sqlStatement->execute();
  }

  private function insertNewOwner(Owner $owner, PDO $connection) {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $sqlStatement = $connection->prepare(
        'INSERT INTO `owners`(`first_name`, `surname`, `address`, `phone_number`, `email`) VALUES (:firstName, :surname, :address, :number, :email)'
      );
  
      $sqlStatement->bindParam(':firstName', $owner->getFirstName(), PDO::PARAM_STR);
      $sqlStatement->bindParam(':surname', $owner->getSurname(), PDO::PARAM_STR);
      $sqlStatement->bindParam(':number', $owner->getPhoneNumber(), PDO::PARAM_STR);
      $sqlStatement->bindParam(':address', $owner->getHomeAddress(), PDO::PARAM_STR);
      $sqlStatement->bindParam(':email', $owner->getEmail(), PDO::PARAM_STR);
  
      $sqlStatement->execute();
      $connection->commit();
    } catch (PDOException $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }

  }

  private function getAllRequirementsForOrder(Order $order, PDO $connection): array {
    $sqlStatement = $connection->prepare(
      'SELECT get_owner_id() AS id_owner'
    );
    $sqlStatement->execute();
    $idOwner = $sqlStatement->fetch(PDO::FETCH_ASSOC);

    $sqlStatement = $connection->prepare(
      'SELECT get_date_id() AS id_date'
    );
    $sqlStatement->execute();
    $idDate = $sqlStatement->fetch(PDO::FETCH_ASSOC);

    $sqlStatement = $connection->prepare(
      'SELECT get_id_pet() AS id_pet'
    );
    $sqlStatement->execute();
    $idPet = $sqlStatement->fetch(PDO::FETCH_ASSOC);

    $sqlStatement = $connection->prepare(
      'SELECT id_caretaker FROM caretakers WHERE id_user = :id'
    );

    $sqlStatement->bindParam(':id', $order->getCaretaker()->getUser()->getId(), PDO::PARAM_INT);

    $sqlStatement->execute();
    $idCaretaker = $sqlStatement->fetch(PDO::FETCH_ASSOC);
    
    return [$idOwner['id_owner'], $idDate['id_date'], $idPet['id_pet'], $idCaretaker['id_caretaker']];
  }

}