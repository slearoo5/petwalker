<?php

require_once 'Controllers/SecurityController.php';
require_once 'Controllers/DefaultController.php';
require_once 'Controllers/SearchController.php';
require_once 'Controllers/SearchResultController.php';
require_once 'Controllers/OrderController.php';
require_once 'Controllers/SettingsController.php';
require_once 'Controllers/ErrorController.php';


class Router {
  private $routes = [];
  
  public function __construct() {
    $this->routes = [
      'landing-page' => [
        'controller' => 'DefaultController',
        'action' => 'welcome'
      ],
      'login-page' => [
        'controller' => 'SecurityController',
        'action' => 'login'
      ],
      'register-page' => [
        'controller' => 'SecurityController',
        'action' => 'register'
      ],
      'search-form' => [
        'controller' => 'SearchController',
        'action' => 'search'
      ],
      'search-results' => [
        'controller' => 'SearchResultController',
        'action' => 'find'
      ],
      'success-page' => [
        'controller' => 'OrderController',
        'action' => 'bookOrder'
      ],
      'orders-list' => [
        'controller' => 'OrderController',
        'action' => 'displayOrders'
      ],
      'order-details' => [
        'controller' => 'OrderController',
        'action' => 'displayOrderDetails'
      ],
      'logout' => [
        'controller' => 'SecurityController',
        'action' => 'logout'
      ],
      'settings-caretaker' => [
        'controller' => 'SettingsController',
        'action' => 'prepare'
      ],
      'page-not-found' => [
        'controller' => 'ErrorController',
        'action' => 'handle'
      ]
    ];
  }

  public function run(): void {
    $page = $this->getPage();

    $controller = $this->routes[$page]['controller'];
    $action = $this->routes[$page]['action'];

    // Make object based on string literal
    $object = new $controller;
    $object->$action();
  }

  private function getPage(): string {
    if (!isset($_GET['page'])) {
      return 'landing-page';
    } else if (!isset($this->routes[$_GET['page']])) {
      return 'page-not-found';
    } else {
      return $_GET['page'];
    }
  }

}

?>