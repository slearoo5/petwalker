<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/order-details.css" />
</head>

<body>
<?php include(dirname(__DIR__).'/header.php'); ?>

<div class="pw-card pw-order-details-card">
<?php if ($action === 'rejected') { ?>
  <div class="pw-card-title">Odrzucono ofertę :-(</div>
  <div class="pw-card-subtitle"><a href="?page=orders-list"><button class="pw-button">Wróć do listy zleceń</button></a></div>
  <div class="pw-card-footer">ID zlecenia: <?php echo $idOrder ?> </div>
  
<?php } else if ($action === 'accepted') { ?>
  <div class="pw-card-title">Zaakceptowano ofertę :-)</div>
  <div class="pw-card-subtitle">
    <br/> Numer telefonu do zleceniodawcy: <?php echo $phoneNumber; ?> <br/>
    <a href="?page=orders-list"><button class="pw-button">Wróć do listy zleceń</button></a>
  </div>
  <div class="pw-card-footer">ID zlecenia: <?php echo $idOrder ?> </div>
<?php  } ?>

</div>

</body>
