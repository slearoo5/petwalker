<?php 

class Date {
  private string $startDay;
  private string $endDay;
  private string $startHour;
  private string $endHour;
  private ?int $id;

  public function build(): Date {
    $instance = new self();
    return $instance;
  }

  public function withStartDay(string $day) {
    $this->startDay = $day;
    return $this;
  }

  public function withEndDay(string $day) {
    $this->endDay = $day;
    return $this;
  }

  public function withStartHour(string $hour) {
    $this->startHour = $hour;
    return $this;
  }

  public function withEndHour(string $hour) {
    $this->endHour = $hour;
    return $this;
  }

  public function withId(int $id) {
    $this->id = $id;
    return $this;
  }

  public function getStartDay(): string {
    return $this->startDay;
  }
  
  public function getEndDay(): string {
    return $this->endDay;
  }

  public function getStartHour(): string {
    return $this->startHour;
  }

  public function getEndHour(): string {
    return $this->endHour;
  }

  public function getId(): int {
    return $this->id;
  }
}

?>