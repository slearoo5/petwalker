<!DOCTYPE html>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/search-form.css" />
  
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="../Public/JavaScript/search-form.js"></script>
</head>

<body>
    <?php include(dirname(__DIR__).'/header.php'); ?>

    <div class="pw-search-form-container">
        <div class="pw-card pw-search-form-card pw-search-form-card-step-two">
            <div class="pw-card-title">Krok 2/3</div>
            <div class="pw-card-subtitle">
                Wypełnij informacje o swoim pupilu
            </div>
            <div class="pw-card-content pw-search-form-card-content">
                <form action="?page=search-form" method="GET">
                    <!-- Adding hidden input to concat url query parameters -->
                    <?php 
                      foreach ($_GET as $key => $value) {
                        echo "<input type='hidden' name=$key value=$value />";
                      }
                    ?>

                    Rodzaj usługi?<br />
                    <select name="serviceType" class="pw-select pw-landing-page-select" required>
                      <option value="full_care">Pełna opieka</option>
                      <option value="service">Jednorazowa usługa (np. sprzątanie terrarium)</option>
                    </select><br /><br />

                    Jakie zwierzę?<br> 
                    <input class="pw-input" type="text" name="breed" required /><br><br>

                    Imię (opcjonalne)<br> 
                    <input class="pw-input" type="text" name="name" required /><br><br>
                    
                    <div class="pw-button-position">
                      <input type="button" class="pw-button pw-search-form-back-button" value="Wstecz" onclick="location.href='?page=search-form';" />
                      <button type="submit" name="step2" value="true" class="pw-button pw-search-form-next-button">Dalej</button>              
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>