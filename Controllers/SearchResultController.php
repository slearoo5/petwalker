<?php 

require_once 'AppController.php';
require_once __DIR__.'//..//Models/Offer.php';
require_once __DIR__.'/../Repository/CaretakerRepository.php';
require_once __DIR__.'/../Services/PriceCalculationService.php';
require_once __DIR__.'/../Services/SearchParamMapper.php';


class SearchResultController extends AppController {
  private PriceCalculationService $calculationService;
  private SearchParamMapper $searchParamMapper;
  
  public function __construct() {
    parent::__construct();
    $this->calculationService = new PriceCalculationService();
    $this->searchParamMapper = new SearchParamMapper();
  }

  public function find() {
    if ($this->isGet()) {
      $caretakerRepository = new CaretakerRepository();
      $restriction = $this->searchParamMapper->getRestrictionParams($_GET['petType']);

      $caretakers = $caretakerRepository->getCaretakerByRestriction($restriction);
    
      $offers = [];
      foreach ($caretakers as $caretaker) {
        $price = $this->calculationService->calculatePrice($caretaker->getStake());
        $offers[] = new Offer($caretaker, $price);
      }
      $_SESSION['offers'] = serialize($offers);
    }

    $this->render('search-results', ['offers' => $offers]);
  }
}

?>