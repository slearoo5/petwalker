<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//Owner.php';

class OwnerRepository extends Repository {

  public function getOwnersByPhoneNumber(string $phoneNumber): array {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $sqlStatement = $connection->prepare(
        'SELECT * FROM owners WHERE phone_number = :phoneNumber'
      );

      $sqlStatement->bindParam(':phoneNumber', $phoneNumber, PDO::PARAM_STR);
      $sqlStatement->execute();
  
      $owners = $sqlStatement->fetchAll(PDO::FETCH_ASSOC);
      $connection->commit();
  
      $result = [];
      foreach ($owners as $owner) {
        $o = Owner::build()
                  ->withId($owner['id_owner'])
                  ->withFirstName($owner['first_name'])
                  ->withSurname($owner['surname'])
                  ->withHomeAddress($owner['address'])
                  ->withPhoneNumber($owner['phone_number'])
                  ->withEmail($owner['email']);
  
        $result[] = $o;
      }
  
      return $result;
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }

  }

}