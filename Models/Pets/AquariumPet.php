<?php


class AquariumPet {
  private $type;
  private ?int $id;

  public function __construct() { }

  public static function build(): AquariumPet {
    $instance = new self();
    return $instance;
  }

  public function getId(): int {
    return $this->id;
  }

  public function getPetType(): string {
    return $this->type;
  }

  public function setPetType(string $petType) {
    $this->type = $petType;
    return $this;
  }

  public function withId(int $id) {
    $this->id = $id;
    return $this;
  }

}

?>