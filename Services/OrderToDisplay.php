<?php

require_once __DIR__.'/../Models/Order.php';

class OrderToDisplay {
  private int $id;
  public string $petType;
  public string $isPetAggressive;
  public string $phoneNumber;
  public string $address;
  public string $startDate;
  public string $endDate;
  public string $startHour;
  public string $endHour;
  public string $jobType;
  public string $offerStatus;
  public int $price;

  public function __construct(Order $order) {
    $this->petType = $this->checkPetType(get_class($order->getPet()));

    if ($this->petType === 'Pies') {
      $this->isPetAggressive = $order->getPet()->isAgressive() ? 'Tak' : 'Nie';
    } else if ($this->petType === 'Kot') {
      $this->isPetAggressive = $order->getPet()->isObedient() ? 'Nie' : 'Tak';
    } else {
      $this->isPetAggressive = 'Nie';
    }

    $this->phoneNumber = $order->getOwner()->getPhoneNumber();
    $this->address = $order->getOwner()->getHomeAddress();
    $this->startDate = $order->getDate()->getStartDay();
    $this->endDate = $order->getDate()->getEndDay();
    $this->startHour = $order->getDate()->getStartHour();
    $this->endHour = $order->getDate()->getEndHour();
    $this->jobType = $this->checkServiceType($order->getServiceType());
    $this->offerStatus = $this->checkStatus($order->getStatus());
    $this->price = $order->getPrice();
    $this->id = $order->getId();
  }

  public function getId(): int {
    return $this->id;
  }

  private function checkStatus(string $status): string {
    switch ($status) {
      case 'pending':
        return 'Nowe';
      break;

      case 'accepted':
        return 'Zaakceptowane';
      break;

      case 'rejected':
        return 'Odrzucone';
      break;
    }
  }


  private function checkServiceType(string $service): string {
    switch ($service) {
      case 'full_care':
        return 'Pełna opieka';
      break;

      case 'walk':
        return 'Spacer';
      break;

      case 'service':
        return 'Jednorazowa usługa';
      break;
    }
  }

  private function checkPetType(string $petType): string {
    switch ($petType) {
      case 'Dog':
        return 'Pies';
      break;

      case 'Cat':
        return 'Kot';
      break;

      case 'TerrariumPet':
        return 'Zwierzę/zwierzęta w terrarium';
      break;

      case 'AquariumPet':
        return 'Zwierzę/zwierzęta w akwarium';
      break;
    }
    return $petType;
  }
}

?>