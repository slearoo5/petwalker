
<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models//User.php';

class UserRepository extends Repository {

  public function getUser(string $email): ?User {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $stmt = $connection->prepare('
        SELECT * FROM users WHERE email = :email
      ');
      $stmt->bindParam(':email', $email, PDO::PARAM_STR);
      $stmt->execute();
      $user = $stmt->fetch(PDO::FETCH_ASSOC);

      $connection->commit();
      if($user == false) {
        return null;
      }
      return new User(
        $user['email'],
        $user['password'],
        $user['id_user'],
        $user['first_name'],
        $user['surname'],
        $user['phone_number'],
        $user['home_address']
      );
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }
  }

  public function getUserById(int $id) {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try { 
      $stmt = $connection->prepare('
        SELECT * FROM users WHERE id_user = :id');
      $stmt->bindParam(':id', $id, PDO::PARAM_INT);
      $stmt->execute();
      $user = $stmt->fetch(PDO::FETCH_ASSOC);
      if($user == false) {
        return null;
      }
      $connection->commit();

      return new User(
        $user['email'],
        $user['password'],
        $user['id_user'],
        $user['first_name'],
        $user['surname'],
        $user['phone_number'],
        $user['home_address']
      );
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }
  }

  public function getUsers(): array {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $result = [];
      $stmt = $connection->prepare('
          SELECT * FROM users
      ');
      $stmt->execute();
      $users = $stmt->fetchAll(PDO::FETCH_ASSOC);
      $connection->commit();

      foreach ($users as $user) {
        $result[] = new User(
          $user['email'],
          $user['password'],
          $user['id_user'],
          $user['first_name'] ?: '',
          $user['surname'] ?: '',
          $user['phone_number'] ?: '',
          $user['home_address'] ?: ''
        );
      }
      return $result;
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }
  }

  public function addUser(User $user) {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $sqlStatement = $connection->prepare(
        'INSERT INTO users (email, password, first_name, surname, phone_number, home_address) VALUES (:email, :password, :first_name, :surname, :phone_number, :home_address)'
      );
      $email = $user->getEmail();
      $password = $user->getPassword();
      $firstName = $user->getFirstName();
      $surname = $user->getSurname();
      $phoneNumber = $user->getPhoneNumber();
      $homeAddress = $user->getHomeAddress();
      $sqlStatement->bindParam(':email', $email, PDO::PARAM_STR);
      $sqlStatement->bindParam(':password', $password, PDO::PARAM_STR);
      $sqlStatement->bindParam(':first_name', $firstName, PDO::PARAM_STR);
      $sqlStatement->bindParam(':surname', $surname, PDO::PARAM_STR);
      $sqlStatement->bindParam(':phone_number', $phoneNumber, PDO::PARAM_STR);
      $sqlStatement->bindParam(':home_address', $homeAddress, PDO::PARAM_STR);
      $sqlStatement->execute();

      $connection->commit();
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->getMessage();
    }
  }

}