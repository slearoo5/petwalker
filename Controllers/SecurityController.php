<?php 

require_once 'AppController.php';
require_once __DIR__.'/../Models/User.php';
require_once __DIR__.'/../Repository/UserRepository.php';


class SecurityController extends AppController {

  public function login() {
    $userRepository = new UserRepository();
    $users = $userRepository->getUsers();

    if ($this->isPost()) {
      foreach ($users as $user) {
        if ($user->getEmail() === $_POST['email'] && password_verify($_POST['password'], $user->getPassword())) {
          $_SESSION['id'] = $user->getId();
          $_SESSION['email'] = $user->getEmail();
                
          $url = "http://$_SERVER[HTTP_HOST]/";
          header("Location: {$url}?page=settings-caretaker");
  
          return;
        }
      }

      $this->render('login-page', ['messages' => ['Adres e-mail lub hasło jest niepoprawne!'] ]); 
      
    } else {
      $this->render('login-page');
    }

  }

  public function logout() {
    session_unset();
    session_destroy();

    $this->render('login-page', ['messages' => ['Pomyślnie wylogowano! Zapraszamy ponownie :-)']]);
  }

  public function register() {

    if ($this->isPost()) {

      if ($_POST['password'] === $_POST['passwordConfirmation']) {

        $userRepository = new UserRepository();
        $users = $userRepository->getUsers();

        foreach ($users as $registeredUser) {
          if ($registeredUser->getEmail() === $_POST['email']) {
            $this->render('register-page', ['messages' => ['Użytkownik o takim adresie e-mail już istnieje!']]);

            return;
          }
        }

        $userRepository->addUser(new User(
          $_POST['email'],
          password_hash($_POST['password'], PASSWORD_DEFAULT)
        ));
        $this->login();

        return;
      } else {
        $this->render('register-page', ['messages' => ['Hasła nie zgadzają się']]);

        return;
      }
    } else {
      $this->render('register-page');
    }

  }

}

?>