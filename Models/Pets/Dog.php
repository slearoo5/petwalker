<?php


class Dog {
  private $name;
  private $breed;
  private $sex;
  private $weight;
  private $aggressive;
  private ?int $id;

  public function __construct() { }

  public static function build(): Dog {
    $instance = new self();
    return $instance;
  }

  public function getName(): string {
    return $this->name;
  }

  public function getBreed(): string {
    return $this->breed;
  }

  public function getSex(): string {
    return $this->sex;
  }

  public function getWeight(): int {
    return $this->weight;
  }

  public function isAgressive(): bool {
    return $this->aggressive;
  }

  public function getId(): int {
    return $this->id;
  }

  public function setName(string $name) {
    $this->name = $name;
    return $this;
  }

  public function setBreed(string $breed) {
    $this->breed = $breed;
    return $this;
  }

  public function setSex(string $sex) {
    $this->sex = $sex;
    return $this;
  }

  public function setWeight(int $weight) {
    $this->weight = $weight;
    return $this;
  }

  public function setIsAgressive(bool $aggressive) {
    $this->aggressive = $aggressive;
    return $this;
  }

  public function withId(int $id) {
    $this->id = $id;
    return $this;
  }
}

?>