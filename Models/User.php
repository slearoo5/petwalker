<?php

class User {

  private string $email;
  private string $password;
  private ?string $firstName;
  private ?string $surname;
  private ?string $phoneNumber;
  private ?string $homeAddress;
  private ?int $id;
  
  public function __construct(
      string $email,
      string $password,
      int $id = null,
      string $firstName = '',
      string $surname = '',
      string $phoneNumber = '',
      string $homeAddress = '') {

    $this->email = $email;
    $this->password = $password;
    $this->id = $id;
    $this->firstName = $firstName;
    $this->surname = $surname;
    $this->phoneNumber = $phoneNumber;
    $this->homeAddress = $homeAddress;
  }
  
  public function getEmail(): string {
    return $this->email; 
  }

  public function getPassword(): string {
    return $this->password;
  }

  public function getId(): int {
    return $this->id;
  }

  public function getFirstName(): string {
    return $this->firstName;
  }

  public function getSurname(): string {
    return $this->surname;
  }

  public function getPhoneNumber(): string {
    return $this->phoneNumber;
  }

  public function getHomeAddress(): string {
    return $this->homeAddress;
  }

}
