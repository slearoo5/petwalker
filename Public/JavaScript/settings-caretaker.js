function addNewSchedule() {
  var template = `
  <div class="pw-card">
    Dzień i godziny pracy:<br/>  
    <select name="weekDay[]" class="pw-select">
      <option value="Mon">Poniedziałek</option>
      <option value="Tue">Wtorek</option>
      <option value="Wed">Środa</option>
      <option value="Thu">Czwartek</option>
      <option value="Fri">Piątek</option>
      <option value="Sat">Sobota</option>
      <option value="Sun">Niedziela</option>
    </select>&nbsp;

    Cały dzień?
    <input name="allDay[]" type="checkbox" value="1" /><br/>
    
    <i class="fa fa-clock"></i>&nbsp; Od:<input type="time" name="startTime[]" step="3600" class="pw-input" />&nbsp;&nbsp;
    <i class="fa fa-clock"></i>&nbsp; Do:<input type="time" name="endTime[]" step="3600" class="pw-input" />
  </div>
  `;

  $('.pw-week-days-conatiner').append(template);
}
