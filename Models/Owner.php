<?php

class Owner {
  private string $firstName;
  private string $surname;
  private string $homeAddress;
  private string $phoneNumber;
  private string $email;
  private ?int $id;

  public static function build(): Owner {
    $instance = new self();
    return $instance;
  }

  public function withFirstName(string $name) {
    $this->firstName = $name;
    return $this;
  }

  public function withSurname(string $surname) {
    $this->surname = $surname;
    return $this;
  }

  public function withHomeAddress(string $address) {
    $this->homeAddress = $address;
    return $this;
  }

  public function withPhoneNumber(string $number) {
    $this->phoneNumber = $number;
    return $this;
  }

  public function withEmail(string $email) {
    $this->email = $email;
    return $this;
  }

  public function withId(int $id) {
    $this->id = $id;
    return $this;
  }

  public function getId(): ?int {
    return $this->id;
  }

  public function getFirstName(): string {
    return $this->firstName;
  }

  public function getSurname(): string {
    return $this->surname;
  }

  public function getPhoneNumber(): string {
    return $this->phoneNumber;
  }

  public function getHomeAddress(): string {
    return $this->homeAddress;
  }

  public function getEmail(): string {
    return $this->email; 
  }

}
