<?php 

require_once 'AppController.php';
require_once __DIR__.'/../Models/Pets/Dog.php';
require_once __DIR__.'/../Models/Pets/Cat.php';
require_once __DIR__.'/../Models/Pets/AquariumPet.php';
require_once __DIR__.'/../Models/Pets/TerrariumPet.php';
require_once __DIR__.'/../Models/Order.php';
require_once __DIR__.'/../Services/OrderToDisplay.php';
require_once __DIR__.'/../Services/OrderMapper.php';
require_once __DIR__.'/../Repository/CaretakerRepository.php';
require_once __DIR__.'/../Repository/OrderRepository.php';

class OrderController extends AppController {
  private OrderMapper $orderMapper;

  public function __construct() {
    parent::__construct();
    $this->orderMapper = new OrderMapper();
  }
  
  public function displayOrderDetails() {
    if (isset($_SESSION['id'])) {
      $orderRepository = new OrderRepository();

      $tmp = explode('_', $_POST['action_id']);
      $action = $tmp[0];
      $id = $tmp[1];
      $phoneNumber = isset($tmp[2]) ? $tmp[2] : '';

      $orderRepository->updateOrder($id, $action);

      $this->render('order-details', ['action' => $action, 'idOrder' => $id, 'phoneNumber' => $phoneNumber]);
    }
  }

  public function displayOrders() {
    if (isset($_SESSION['id'])) {
      $orderRepository = new OrderRepository();
      $orders = $orderRepository->getOrdersByCaretakerId($_SESSION['id']);

      $ordersToDisplay = [];
      foreach ($orders as $o) {
        $ordersToDisplay[] = new OrderToDisplay($o);
      }
      usort($ordersToDisplay, fn($a, $b) => $b->getId() - $a->getId());

      $this->render('orders-list', ['orders' => $ordersToDisplay]);
    } else {
      echo 'Nie masz uprawnień, aby obejrzeć tą stronę!';
    }
  }

  public function bookOrder() {
    if (isset($_POST)) {
      $orderRepository = new OrderRepository();

      $pet = $this->orderMapper->mapPet();
      $owner = $this->orderMapper->mapToOwner();
      $caretaker = $this->orderMapper->mapToCaretaker();
      $date = $this->orderMapper->maptoDate();
      $price = $_POST['price'];
      $serviceType = $_POST['serviceType'];

      $order = Order::build()
                ->withPet($pet)
                ->withOwner($owner)
                ->withCaretaker($caretaker)
                ->withDate($date)
                ->withPrice($price)
                ->withServiceType($serviceType)
                ->withStatus('pending');

      $orderRepository->addNewOrder($order);  
    }

    $this->render('success-page');
  }

}
?>