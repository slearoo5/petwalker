<?php

require_once "Repository.php";
require_once __DIR__.'//..//Models/Caretaker/Restriction.php';
require_once __DIR__.'//..//Models/Caretaker/Stake.php';
require_once __DIR__.'//..//Models/Caretaker/WorkSchedule.php';
require_once __DIR__.'//..//Models/Caretaker/Caretaker.php';


class CaretakerRepository extends Repository {

  public function addInitialCaretaker(int $userId) {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $sqlStatement = $connection->prepare(
        'INSERT INTO caretakers (id_user) VALUES (:id)'
      );
  
      $sqlStatement->bindParam(':id', $userId, PDO::PARAM_INT);
      $sqlStatement->execute();
  
      $sqlStatement = $connection->prepare(
        'SELECT id_caretaker FROM caretakers WHERE id_user = :id'
      );
      $sqlStatement->bindParam(':id', $userId, PDO::PARAM_INT);
      $sqlStatement->execute();
      $caretakerId = $sqlStatement->fetch(PDO::FETCH_ASSOC);
  
      $sqlStatement = $connection->prepare(
        'INSERT INTO daily_work_schedule (id_caretaker) VALUES (:id)'
      );
      $sqlStatement->bindParam(':id', $caretakerId, PDO::PARAM_INT);
      $sqlStatement->execute();

      $connection->commit();
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }
  }

  public function getCaretakerById(int $id): ?Caretaker {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $sqlStatement = $connection->prepare(
        'SELECT * FROM registered_users WHERE id_user = :id'
      );
      $sqlStatement->bindParam(':id', $id, PDO::PARAM_INT);
      $sqlStatement->execute();
      $caretaker = $sqlStatement->fetch(PDO::FETCH_ASSOC);

      $connection->commit();

      $p = explode(' ', $caretaker['pet_type']);
      $s = explode(' ', $caretaker['service_type']);
  
      $restr = Restriction::build()
                ->withPetTypes($p)
                ->withServiceTypes($s)
                ->withAggressivePets($caretaker['aggressive_pet'])
                ->withMaxWeight($caretaker['max_pet_weight']);
  
      $stake = Stake::build()
                ->withFullCare($caretaker['stake_full_care'])
                ->withWalk($caretaker['stake_walk'])
                ->withService($caretaker['stake_service'])
                ->withAggressivePenalty($caretaker['stake_aggressive_pet']);
  
      $user = new User(
        $caretaker['email'],
        $caretaker['password'],
        $caretaker['id_user'],
        $caretaker['first_name'],
        $caretaker['surname'],
        $caretaker['phone_number'],
        $caretaker['home_address']
      );
      
      $result = new Caretaker($restr, $stake, [], $caretaker['first_name'], $user, !!$caretaker['active']);
      return $result;
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }
  }

  public function getCaretakerByEmail(string $email): ?Caretaker {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $statement = $connection
                        ->prepare("SELECT u.*, c.*, GROUP_CONCAT(d.day_of_week, ' ', d.all_day, ' ', d.hour_start, ' ', d.hour_end) FROM caretakers c 
                          JOIN users u ON c.id_user = u.id_user
                          JOIN daily_work_schedule d ON d.id_caretaker = c.id_caretaker
                          WHERE u.email = :email GROUP BY c.id_caretaker");

      $statement->bindParam(':email', $_SESSION['email'], PDO::PARAM_STR);
      $statement->execute();

      $caretaker = $statement->fetch(PDO::FETCH_ASSOC);

      $connection->commit();

      $p = explode(' ', $caretaker['pet_type']);
      $s = explode(' ', $caretaker['service_type']);

      $restr = Restriction::build()
      ->withPetTypes($p)
      ->withServiceTypes($s)
      ->withAggressivePets($caretaker['aggressive_pet'])
      ->withMaxWeight($caretaker['max_pet_weight']);

      $stake = Stake::build()
      ->withFullCare($caretaker['stake_full_care'])
      ->withWalk($caretaker['stake_walk'])
      ->withService($caretaker['stake_service'])
      ->withAggressivePenalty($caretaker['stake_aggressive_pet']);

      $nickname = $caretaker['first_name'];
      $workSchedules = $this->parseWorkSchedule($caretaker["GROUP_CONCAT(d.day_of_week, ' ', d.all_day, ' ', d.hour_start, ' ', d.hour_end)"]);
      $user = new User(
      $caretaker['email'],
      $caretaker['password'],
      $caretaker['id_user'],
      $caretaker['first_name'],
      $caretaker['surname'],
      $caretaker['phone_number'],
      $caretaker['home_address']
      );

      $result = new Caretaker($restr, $stake, $workSchedules, $nickname, $user, !!$caretaker['active']);
      return $result;

    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }

  }

  public function getCaretakerByRestriction(Restriction $restriction): array {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $result = [];

      $services = $restriction->getServiceTypes();
      $service = $services[0];
  
      $petTypes = $restriction->getPetTypes();
      $petType = $petTypes[0];
  
      $isAggressive = $restriction->getCareAggressivePets();
  
      $maxWeight = $restriction->getMaxWeight();
  
      $statement = $connection
            ->prepare("SELECT c.stake_full_care, c.stake_walk, c.stake_service, c.stake_aggressive_pet, c.pet_type, c.service_type, c.aggressive_pet, c.max_pet_weight, u.*, u.first_name, u.surname, GROUP_CONCAT(d.day_of_week, ' ', d.all_day, ' ', d.hour_start, ' ', d.hour_end)
                    FROM caretakers c 
                    JOIN users u ON c.id_user = u.id_user
                    JOIN daily_work_schedule d ON c.id_caretaker = d.id_caretaker
                    WHERE c.active > 0
                      AND c.pet_type LIKE :pet
                      AND c.service_type LIKE :service
                      AND c.aggressive_pet >= :aggressive
                      AND c.max_pet_weight >= :weight
                        GROUP BY c.id_caretaker");
  
      $statement->bindValue(':pet', '%'.$petType.'%', PDO::PARAM_STR);
      $statement->bindValue(':service', '%'.$service.'%', PDO::PARAM_STR);
      $statement->bindValue(':aggressive', $isAggressive, PDO::PARAM_INT);
      $statement->bindParam(':weight', $maxWeight, PDO::PARAM_INT);
  
      $statement->execute();
      $caretakers = $statement->fetchAll(PDO::FETCH_ASSOC);
      $connection->commit();

      $result = [];
      foreach ($caretakers as $ct) {
        $p = explode(' ', $ct['pet_type']);
        $s = explode(' ', $ct['service_type']);
  
        $restr = Restriction::build()
                  ->withPetTypes($p)
                  ->withServiceTypes($s)
                  ->withAggressivePets($ct['aggressive_pet'])
                  ->withMaxWeight($ct['max_pet_weight']);
  
        $stake = Stake::build()
                  ->withFullCare($ct['stake_full_care'])
                  ->withWalk($ct['stake_walk'])
                  ->withService($ct['stake_service'])
                  ->withAggressivePenalty($ct['stake_aggressive_pet']);
  
        $user = new User($ct['email'], '', $ct['id_user'], $ct['first_name'], $ct['surname'], $ct['phone_number'], $ct['home_address']);
  
        $workSchedules = $this->parseWorkSchedule($ct["GROUP_CONCAT(d.day_of_week, ' ', d.all_day, ' ', d.hour_start, ' ', d.hour_end)"]);
        $nickname = $ct['first_name'];
        
        $result[] = new Caretaker($restr, $stake, $workSchedules, $nickname, $user, 1);
      }
  
      return $result;
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }
  }

  private function parseWorkSchedule(string $workSchedulesString): array {
    $result = [];
    $workSchedules = explode(',', $workSchedulesString);

    foreach ($workSchedules as $day) {
      if (substr($day, 4) == 1) {
        $result[] = WorkSchedule::build()->withDay(substr($day, 0, 3))->withAllDay(true)->withStartHour('00:00')->withEndHour('23:59');
      } else {
        $result[] = WorkSchedule::build()->withDay(substr($day, 0, 3))->withAllDay(false)->withStartHour(substr($day, 6, 5))->withEndHour(substr($day, 12, 5));
      }
    }

    return $result;
  }


  public function updatePersonalData() {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $statement = $connection
              ->prepare("UPDATE `users` 
                  SET `email`=:email, `first_name`=:firstName, `surname`=:surname, `phone_number`=:phoneNumber, `home_address`=:address
                    WHERE id_user = :id");

      $statement->bindParam(':email', $_POST['email'], PDO::PARAM_STR);
      $statement->bindParam(':firstName', $_POST['firstName'], PDO::PARAM_STR);
      $statement->bindParam(':surname', $_POST['surname'], PDO::PARAM_STR);
      $statement->bindParam(':phoneNumber', $_POST['phoneNumber'], PDO::PARAM_STR);
      $statement->bindParam(':address', $_POST['address'], PDO::PARAM_STR);
      $statement->bindParam(':id', $_SESSION['id'], PDO::PARAM_INT);


      $statement->execute();
      $connection->commit();
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }

  }

  public function updatePreferences() {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $statement = $connection
                      ->prepare("SELECT * FROM caretakers c JOIN users u ON c.id_user = u.id_user WHERE u.id_user = :id");

      $statement->bindParam(':id', $_SESSION['id'], PDO::PARAM_INT);
      $statement->execute();
      $caretaker = $statement->fetch(PDO::FETCH_ASSOC);

      $services = isset($_POST['service']) ? implode(' ', $_POST['service']) : '';
      $petTypes = isset($_POST['petType']) ? implode(' ', $_POST['petType']) : '';
      $aggressive = isset($_POST['aggressive']) ? 1 : 0;
      $active = isset($_POST['profileActive']) ? 1 : 0;

      $statement = $connection
                      ->prepare("UPDATE `caretakers` 
                        SET `pet_type`=:petTypes, `service_type`=:services, `max_pet_weight`=:maxWeight, `aggressive_pet`=:aggressive, `active`=:active
                        WHERE id_user = :id");

      $statement->bindParam(':id', $_SESSION['id'], PDO::PARAM_INT);
      $statement->bindParam(':petTypes', $petTypes, PDO::PARAM_STR);
      $statement->bindParam(':services', $services, PDO::PARAM_STR);
      $statement->bindParam(':aggressive', $aggressive, PDO::PARAM_INT);
      $statement->bindParam(':maxWeight', $_POST['weight'], PDO::PARAM_STR);
      $statement->bindParam(':active', $active, PDO::PARAM_BOOL);
      $statement->execute();

      // delete all daily schedules
      $statement = $connection
                      ->prepare("DELETE FROM `daily_work_schedule` WHERE id_caretaker = :id");
      $statement->bindParam(':id', $caretaker['id_caretaker'], PDO::PARAM_INT);
      $statement->execute();

      // insert new ones
      $weekDays = $_POST['weekDay'];
      $allDays = $_POST['allDay'];
      $startTimes = $_POST['startTime'];
      $endTimes = $_POST['endTime'];

      $statement = $connection
                        ->prepare("INSERT INTO daily_work_schedule (all_day, day_of_week, hour_start, hour_end, id_caretaker) VALUES (:allDay, :weekDay, :startHour, :endHour, :id)");

      for ($i = 0; $i < count($_POST['weekDay']); $i++) {
        $statement->bindValue(':allDay', $allDays[$i] ?: 0, PDO::PARAM_INT);
        $statement->bindValue(':weekDay', $weekDays[$i], PDO::PARAM_STR);
        $statement->bindValue(':startHour', $startTimes[$i] ?: '00:00', PDO::PARAM_STR);
        $statement->bindValue(':endHour', $endTimes[$i] ?: '23:59', PDO::PARAM_STR);
        $statement->bindParam(':id', $caretaker['id_caretaker'], PDO::PARAM_INT);
        $statement->execute();
      }

      $connection->commit();
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }
  }

  public function updateStakes() {
    $connection = $this->database->connect();
    $connection->beginTransaction();

    try {
      $statement = $connection
                      ->prepare("UPDATE `caretakers` 
                                          SET `stake_full_care`=:fullCare, `stake_walk`=:walk, `stake_service`=:service, `stake_aggressive_pet`=:aggressive
                                          WHERE id_user = :id");

      $statement->bindParam(':id', $_SESSION['id'], PDO::PARAM_INT);
      $statement->bindParam(':fullCare', $_POST['stakeFullCare'], PDO::PARAM_INT);
      $statement->bindParam(':walk', $_POST['stakeWalk'], PDO::PARAM_INT);
      $statement->bindParam(':service', $_POST['stakeService'], PDO::PARAM_INT);
      $statement->bindParam(':aggressive', $_POST['stakeAggressive'], PDO::PARAM_INT);
      $statement->execute();

      $connection->commit();
    } catch (PDO $e) {
      echo $e->getMessage();
      $connection->rollBack();
    }
  }

  public function updatePassword() {
    if ($_POST['password'] === $_POST['passwordConfirm']) {
      $connection = $this->database->connect();
      $connection->beginTransaction();

      try {
        $statement = $connection
                        ->prepare("UPDATE `users` 
                          SET `password`=:password
                          WHERE id_user = :id");
        
        $statement->bindParam(':id', $_SESSION['id'], PDO::PARAM_INT);
        $hash = password_hash($_POST['password'], PASSWORD_DEFAULT);
        $statement->bindParam(':password', $hash, PDO::PARAM_STR);
  
        $statement->execute();
        $connection->commmit();
      } catch (PDO $e) {
        echo $e->getMessage();
        $connection->rollBack();
      }

    } else {
      echo 'Hasła się nie zgadzają!';
    }
  }
}

?>