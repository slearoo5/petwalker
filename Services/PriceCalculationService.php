<?php

require_once __DIR__.'//..//Models//Caretaker//Stake.php';
require_once __DIR__.'//..//Models//OfferServices.php';


class PriceCalculationService {
  public function calculatePrice(Stake $stake): int {
    $totalPrice = 0;
    $service = $_GET['serviceType'];

    switch ($service) {
      case OfferServices::FULL_CARE:
        $totalPrice += $stake->getFullCare();
      break;

      case OfferServices::WALK:
        $totalPrice += $stake->getWalk();
      break;

      case OfferServices::SERVICE:
        $totalPrice += $stake->getService();
      break;
    }
    
    if (isset($_GET['aggressive']) || isset($_GET['obedient'])) {
      $totalPrice += $stake->getAggressivePenalty(); 
    }

    return $totalPrice;
  }

}

?>