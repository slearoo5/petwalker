<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0">
    <!-- Custom CSS styles -->
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/styles.css" />
    <link rel="Stylesheet" type="text/css" href="../Public/Styles/settings-caretaker.css" />
    
    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="../Public/JavaScript/settings-caretaker.js"></script>

</head>

<body>
<?php include(dirname(__DIR__).'/header.php'); ?>


    <div class="pw-settings-caretaker-container">

        <div class="pw-card pw-card-settings-caretaker">
            <div class="pw-card-title pw-card-title-settings-caretaker">Konto opiekuna</div>

            <div class="pw-card-content pw-card-grid-settings-caretaker pw-card-content-settings-caretaker">

                <div>
                <form action="?page=settings-caretaker&section=personalData" method="POST">

                    <div class="pw-card-subtitle pw-card-subtitle-settings-caretaker">Dane personalne</div>

                        E-mail<br> 
                        <input class="pw-input" type="email" name="email" value="<?php echo $user->getUser()->getEmail(); ?>" required /><br><br>
                        
                        Imię<br> 
                        <input class="pw-input" type="text" name="firstName" value="<?php echo $user->getUser()->getFirstName(); ?>" required /><br><br>
    
                        Nazwisko<br> 
                        <input class="pw-input" type="text" name="surname" value="<?php echo $user->getUser()->getSurname(); ?>" required /><br><br> 
                        
                        Adres<br> 
                        <input class="pw-input" type="text" name="address" value="<?php echo $user->getUser()->getHomeAddress(); ?>" required /><br><br>
    
                        Telefon<br> 
                        <input class="pw-input" type="tel" max-lenght="15" name="phoneNumber" value="<?php echo $user->getUser()->getPhoneNumber(); ?>" required /><br><br>

                        <button type="submit" class="pw-button pw-button-settings-caretaker">Zapisz</button>   
                  </form>           

                </div>

                <div>
                <form action="?page=settings-caretaker&section=preferences" method="POST">

                    <div class="pw-card-subtitle pw-card-subtitle-settings-caretaker">Preferencje</div>   
                    Aktywne konto opiekuna: 
                        <input type="checkbox" name="profileActive" <?php if($user->getActive()) {echo 'checked';} ?>></input><br><br />

                      <div class="pw-week-days-conatiner">
                      Grafik pracy:<br/>
                      <?php 
                        foreach ($user->getWorkSchedules() as $workSchedule) { 
                      ?>
                        <div class="pw-card">
                          Dzień i godziny pracy:<br/>
                          <select name="weekDay[]" class="pw-select" required>
                            <option value="Mon" <?php if($workSchedule->getDay() === 'Mon') echo 'selected'?> >Poniedziałek</option>
                            <option value="Tue" <?php if($workSchedule->getDay() === 'Tue') echo 'selected'?>>Wtorek</option>
                            <option value="Wed" <?php if($workSchedule->getDay() === 'Wed') echo 'selected'?>>Środa</option>
                            <option value="Thu" <?php if($workSchedule->getDay() === 'Thu') echo 'selected'?>>Czwartek</option>
                            <option value="Fri" <?php if($workSchedule->getDay() === 'Fri') echo 'selected'?>>Piątek</option>
                            <option value="Sat" <?php if($workSchedule->getDay() === 'Sat') echo 'selected'?>>Sobota</option>
                            <option value="Sun" <?php if($workSchedule->getDay() === 'Sun') echo 'selected'?>>Niedziela</option>
                          </select>&nbsp;

                          Cały dzień?
                          <input name="allDay[]" type="checkbox" value="1" <?php if($workSchedule->getAllDay()) echo 'checked'?> /><br/>
                          
                          <i class="fa fa-clock"></i>&nbsp; Od:<input type="time" name="startTime[]" step="3600" class="pw-input" value="<?php echo $workSchedule->getStartHour();?>" />&nbsp;&nbsp;
                          <i class="fa fa-clock"></i>&nbsp; Do:<input type="time" name="endTime[]" step="3600" class="pw-input" value="<?php echo $workSchedule->getEndHour();?>" />
                        </div>

                        <?php } ?>
                      </div>

                      <button type="button" name="newSchedules" onclick="addNewSchedule()" class="pw-button">Dodaj kolejny dzień</button><br/><br/>

                        Rodzaje zwierząt do opieki<br>
                        <input type="checkbox" name="petType[]" value="Dog" <?php if(in_array('Dog', $user->getRestriction()->getPetTypes())) echo 'checked';?> >Psy</input><br>
                        <input type="checkbox" name="petType[]" value="Cat" <?php if(in_array('Cat', $user->getRestriction()->getPetTypes())) echo 'checked';?>>Koty</input><br>
                        <input type="checkbox" name="petType[]" value="TerrariumPet" <?php if(in_array('TerrariumPet', $user->getRestriction()->getPetTypes())) echo 'checked';?>>Zwierzęta w terrarium</input><br>
                        <input type="checkbox" name="petType[]" value="AquariumPet" <?php if(in_array('AquariumPet', $user->getRestriction()->getPetTypes())) echo 'checked';?>>Zwierzęta w akwarium</input>
                        <br><br>

                        Rodzaje usług:<br>
                        <input type="checkbox" name="service[]" value="full_care" <?php if(in_array('full_care', $user->getRestriction()->getServiceTypes())) echo 'checked';?> >Pełna opieka u zleceniodawcy (karmienie, sprzątanie, wyprowadzanie itp.)</input><br>
                        <input type="checkbox" name="service[]" value="walk" <?php if(in_array('walk', $user->getRestriction()->getServiceTypes())) echo 'checked';?>>Spacery</input><br>
                        <input type="checkbox" name="service[]" value="service" <?php if(in_array('service', $user->getRestriction()->getServiceTypes())) echo 'checked';?>>Jednorazowa usługa u zleceniodawcy</input><br>
                        <br><br>

                        Maksymalna waga (w kg)<br>
                        <input class="pw-input" type="number" max="80" min="0" name="weight" value="<?php echo $user->getRestriction()->getMaxWeight(); ?>" required/><br><br>
    
                        Zlecenia z agresywnymi zwierzętami?<br>
                        <input type="radio" name="aggressive" value="1" <?php if($user->getRestriction()->getCareAggressivePets()) { echo 'checked'; } ?> required/>Tak
                        <input type="radio" name="aggressive" value="0" <?php if(!$user->getRestriction()->getCareAggressivePets()) { echo 'checked'; } ?> required/>Nie<br><br>

                        <button type="submit" class="pw-button pw-button-settings-caretaker">Zapisz</button>   
                </form>
                </div>

                <div>
                <form action="?page=settings-caretaker&section=stakes" method="POST">
                    <div class="pw-card-subtitle pw-card-subtitle-settings-caretaker">Stawki</div>
                        Pełna opieka u zleceniodawcy (dzień)<br>
                        <input name="stakeFullCare" class="pw-input" type="number" value="<?php echo $user->getStake()->getFullCare(); ?>" /><br><br>
                        Spacer (30 minut)<br>
                        <input name="stakeWalk" class="pw-input" type="number" value="<?php echo $user->getStake()->getWalk(); ?>" />
                        <br><br>
                        Jednorazowa usługa u zleceniodawcy<br>
                        <input name="stakeService" class="pw-input" type="number" value="<?php echo $user->getStake()->getService(); ?>" />
                        <br><br>
                        Dodatkowa stawka za agresywne zwierzę:<br>
                        <input name="stakeAggressive" class="pw-input" type="number" value="<?php echo $user->getStake()->getAggressivePenalty(); ?>" /><br><br>
                        <button type="submit" class="pw-button pw-button-settings-caretaker">Zapisz</button>   
                </form>
                </div>

                <div>
                <form action="?page=settings-caretaker&section=password" method="POST">
                    <div class="pw-card-subtitle pw-card-subtitle-settings-caretaker">Zmiana hasła</div>
                    Nowe hasło<br> 
                    <input class="pw-input" type="password" name="password" required /><br><br> 
                    Powtórz nowe hasło<br> 
                    <input class="pw-input" type="password" name="passwordConfirm" required /><br><br> 
                    <button type="submit" class="pw-button pw-button-settings-caretaker">Zapisz</button>   
                </form>
                </div>

            </div>
        </div>
    </div>
    
</body>