function showMenu() {
    var x = document.getElementById("pw-mobile-nav")
    if (x.style.display === "block") {
        x.style.display = "none";
    } else {
        x.style.display = "block";
    }
}

function animeHowItWorksCards() {
  $(function() {
    $('#stepOne').toggle();
    $('#stepOne').toggle(800);

    $('#stepTwo').toggle();
    $('#stepTwo').toggle(1400);

    $('#stepThree').toggle();
    $('#stepThree').toggle(2000);
  });
}