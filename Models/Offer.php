<?php 

require_once __DIR__.'//..//Models/Caretaker/Caretaker.php';

class Offer {
  private Caretaker $caretaker;
  private int $price;

  public function __construct(Caretaker $caretaker, int $price) {
    $this->caretaker = $caretaker;
    $this->price = $price;
  }

  public function getCaretaker(): Caretaker {
    return $this->caretaker;
  }

  public function getPrice(): int {
    return $this->price;
  }
}


?>