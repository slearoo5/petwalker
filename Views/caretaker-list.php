<?php
  require_once __DIR__.'//..//Models/Offer.php';

  session_start();

  $offers = unserialize($_SESSION['offers']);

  function displayOffer(Offer $offer, int $offerDuration) {
      echo '<div class="pw-card pw-card-row">
                <span>'.$offer->getCaretaker()->getNickname().'</span>
                <span><i class="fa fa-money-bill-wave"></i>&nbsp;'.$offer->getPrice() * $offerDuration.' PLN</span>
                <input type="hidden" name="price" value="'.$offer->getPrice() * $offerDuration.'" />
                <span><button type="submit" name="idUser" value="'.$offer->getCaretaker()->getUser()->getId().'" class="pw-button">Wyślij &nbsp;<i class="fa fa-paper-plane"></i></button></span><form>
            </div>';
  }

  function getWeekDays(int $firstDayIndex, int $length): array {
    return array_slice(['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'], $firstDayIndex, $length);
  }

  function nextDay(string $weekDay) {
    $nextDay = '';

    switch ($weekDay) {
      case 'Mon':
        $nextDay = 'Tue';
      break;
      
      case 'Tue':
        $nextDay = 'Wed';
      break;

      case 'Wed':
        $nextDay = 'Thu';
      break;

      case 'Thu':
        $nextDay = 'Fri';
      break;

      case 'Fri':
        $nextDay = 'Sat';
      break;

      case 'Sat':
        $nextDay = 'Sun';
      break;

      case 'Sun':
        $nextDay = 'Mon';
      break;
    }

    return $nextDay;
  }

  function getWeekDayIndex(string $weekDay): int {
    switch ($weekDay) {
      case 'Mon':
        $index = 0;
      break;
      
      case 'Tue':
        $index = 1;
      break;

      case 'Wed':
        $index = 2;
      break;

      case 'Thu':
        $index = 3;
      break;

      case 'Fri':
        $index = 4;
      break;

      case 'Sat':
        $index = 5;
      break;

      case 'Sun':
        $index = 6;
      break;
    }

    return $index;
  }

  $startWeekDay = explode('_', $_GET['startDate'])[0];
  $startDate = explode('_', $_GET['startDate'])[1];
  $endWeekDay = explode('_', $_GET['endDate'])[0];
  $endDate = explode('_', $_GET['endDate'])[1];

  $offerDuration = (strtotime($endDate) - strtotime($startDate) + 86400) / 86400;

  $fullDays = (strtotime($endDate) - strtotime($startDate) - 86400) / 86400;

  if(isset($offers)) {
    // 1) check if its the same day
    if (strtotime($startDate) == strtotime($endDate)) {      
      foreach($offers as $offer) {
        $schedule = $offer->getCaretaker()->getWorkSchedules();
        foreach ($schedule as $s) {
          if ($startWeekDay === $s->getDay()) {
            if (strtotime($_GET['startTime']) >= strtotime($s->getStartHour())
                  && strtotime($_GET['endTime']) <= strtotime($s->getEndHour())) {
              displayOffer($offer, $offerDuration);
            }
          }
        }
      }
    }
    // 2) check if 24h <= |endDate - startDay| < 48h (less than two days - not requiring full day!)
    else if ((strtotime($endDate) - strtotime($startDate)) >= 86400  &&  (strtotime($endDate) - strtotime($startDate)) < 172800) {
      $nextDay = nextDay($startWeekDay);
      foreach($offers as $offer) {
        $startOk = false;
        $endOk = false;

        $schedule = $offer->getCaretaker()->getWorkSchedules();
        $workingDays = [];
        foreach ($schedule as $s) {
          $workingDays[] = $s->getDay();
        }
        if (in_array($startWeekDay, $workingDays) && in_array($nextDay, $workingDays)) {
          foreach ($schedule as $s) {
            if ($s->getDay() == $startWeekDay) {
              $startOk = strtotime($_GET['startTime']) >= strtotime($s->getStartHour());
            } else if ($s->getDay() == $nextDay) {
              $endOk = strtotime($_GET['endTime']) <= strtotime($s->getEndHour()) && $s->getStartHour() == '00:00';
            }
          }
        }

        if ($startOk && $endOk) {
          displayOffer($offer, $offerDuration);
        }
      }
    }

    // 3) check if  2day =< endDate - startDate <= 7days (few days, less than week, atleast one full day)
    else if ((strtotime($endDate) - strtotime($startDate)) >= 172800  &&  (strtotime($endDate) - strtotime($startDate)) < 604800 ) {
      $fullDays = (strtotime($endDate) - strtotime($startDate) - 86400) / 86400;
      $indexOfStartDay = getWeekDayIndex($startWeekDay);
      $indexOfFirstBetweenDay = $indexOfStartDay + 1;
      $betweenDays = getWeekDays($indexOfFirstBetweenDay, $fullDays);

      foreach($offers as $offer) {
        $schedule = $offer->getCaretaker()->getWorkSchedules();
        $isStartOk = false;
        $isEndOk = false;
        $isBetweenOk = true;

        $workingDays = [];
        foreach ($schedule as $s) {
          $workingDays[] = $s->getDay();
        }

        if (count(array_intersect($betweenDays, $workingDays)) !== count($betweenDays)) {
          continue;
        }

        for ($i = 0; $i < $fullDays; $i++) {
          if ($isBetweenOk) {

            foreach ($schedule as $s) {
              if ($s->getDay() == $betweenDays[$i]) {
                $isBetweenOk = $s->getAllDay();
              break;
              }
            }
          }
        }

        if (in_array($startWeekDay, $workingDays) && in_array($endWeekDay, $workingDays)) {
          foreach ($schedule as $s) {
            if ($s->getDay() == $startWeekDay) {
              $isStartOk = strtotime($_GET['startTime']) >= strtotime($s->getStartHour());
            } else if ($s->getDay() == $endWeekDay) {
              $isEndOk = strtotime($_GET['endTime']) <= strtotime($s->getEndHour()) && $s->getStartHour() == '00:00';
            }
          }
        }

        if ($isStartOk && $isBetweenOk && $isEndOk) {
          displayOffer($offer, $offerDuration);
        }
      }

    }

    // 4) check if  endDate - startDate > 7days (longer than week)
    else if ((strtotime($endDate) - strtotime($startDate)) >= 604800 ) {
      $weekDays = getWeekDays(0, 7);
      $isWorkingAllWeek = true;

      foreach($offers as $offer) {
        $schedule = $offer->getCaretaker()->getWorkSchedules();

        $workingDays = [];
        foreach ($schedule as $s) {
          $workingDays[] = $s->getDay();
        }

        if (count(array_intersect($weekDays, $workingDays)) !== count($weekDays)) {
          continue;
        }

        foreach ($schedule as $s) {
          if (!$s->getAllDay()) {
            $isWorkingAllWeek = false;
          break;
          }
        }

        if($isWorkingAllWeek) {
          displayOffer($offer, $offerDuration);
        }
      }
    }


    else {
      echo 'To już przeszłość :) Wybierz poprawną datę!';
    }

  }

?>