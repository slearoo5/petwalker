<?php

class Restriction {
  private array $petTypes;
  private array $serviceTypes;
  private bool $aggressivePets;
  private int $maxWeight;

  public function __construct() {
    $this->petTypes = [];
    $this->serviceTypes = [];
    $this->aggressivePets = false;
    $this->maxWeight = 0;
  }

  public static function build() {
    $instance = new self();
    return $instance;
  }

  public function withPetTypes(array $pets) {
    $this->petTypes = $pets;
    return $this;
  }

  public function withServiceTypes(array $services) {
    $this->serviceTypes = $services;
    return $this;
  }

  public function withAggressivePets(bool $aggressive) {
    $this->aggressivePets = $aggressive;
    return $this;
  }

  public function withMaxWeight(int $maxWeight) {
    $this->maxWeight = $maxWeight;
    return $this;
  }

  public function getPetTypes(): array {
    return $this->petTypes;
  }

  public function getServiceTypes(): array {
    return $this->serviceTypes;
  }

  public function getCareAggressivePets(): bool {
    return $this->aggressivePets;
  }

  public function getMaxWeight(): int {
    return $this->maxWeight;
  }
}

?>